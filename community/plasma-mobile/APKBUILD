# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-mobile
pkgver=6.0.0
pkgrel=0
pkgdesc="Modules providing phone functionality for Plasma"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.plasma-mobile.org/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="
	breeze-icons
	dbus-x11
	kpipewire
	kquickcharts
	maliit-keyboard
	plasma-activities
	plasma-nano
	plasma-nm
	plasma-pa
	plasma-settings
	plasma-workspace
	qqc2-breeze-style
	"
makedepends="
	extra-cmake-modules
	kauth-dev
	kbookmarks-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kirigami-addons-dev
	kitemviews-dev
	kjobwidgets-dev
	knotifications-dev
	kpackage-dev
	kpeople-dev
	kservice-dev
	kwayland-dev
	kwidgetsaddons-dev
	kwin-dev
	kwindowsystem-dev
	kxmlgui-dev
	libphonenumber-dev
	libplasma-dev
	modemmanager-qt-dev
	networkmanager-qt-dev
	plasma-activities-dev
	plasma-workspace-dev
	qcoro-dev
	qt6-qtdeclarative-dev
	samurai
	solid-dev
	telepathy-qt-dev
	"

provides="plasma-phone-components=$pkgver-r$pkgrel" # For backwards compatibility
replaces="plasma-phone-components" # For backwards compatibility

subpackages="$pkgname-lang"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/plasma-mobile.git"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-mobile-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5a459a58fcb8ec7c070dec1ca3e519d248239b5c6c6416288717560b2641e14a1e2d1c371bb6d7623b14637641403ffccb3fa351ceeaebfc877903ba4f04e2ea  plasma-mobile-6.0.0.tar.xz
"
